	This repository contains the following: 

	- Contains the deployment, rollout and backout procedure i created for prod deployment. 
  		- The prod deployment is done by other team so i need to provide them the step by step procedure.
  		- The dev team update this every production deployment.  
		  
 	- SQL Scripts to transfer client data from Single Tenant to Multi Tenant Database. The data migration process is as follows: 
  		1. Run TransferNonEvents.  
  		2. Verify if all sql test passed. Example test scenarios is it checks if the count on single tenant and multi tenant are the same.  
  		3. Run TransferEvents.  
  		4. Verify if all sql test passed.  
  

  

