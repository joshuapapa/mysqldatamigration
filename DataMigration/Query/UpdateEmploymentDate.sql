Use `webscale-platform-workforce`;

INSERT INTO events(id,date_start, date_end,state,billable,event_desc,user_id,event_type_id,inserted_at,updated_at,reason_code,shift_template_id,net_duration,task_id,group_id,approved_at,approve_user_id,creation_user_id,update_user_id,link_id,active,notes,source_skills_id, is_all_day)
SELECT unhex(replace(uuid(), '-', '')), 
		'2018-02-18 00:00:00', 
		null, 
		'Approved', 
		0, 
		'Employment dates', 
		u.id, 
		0x33C84339CD7C4F5087A5F39F18B54E2F, now(), now(),null,null,null,null,
		null,null,null,
		null,null,null,1,null,null,0
FROM users u
WHERE u.id not in (
	Select events.user_id from events
	JOIN eventtypes
	ON events.event_type_id = eventtypes.id
	WHERE name = 'Employment Dates'
);
