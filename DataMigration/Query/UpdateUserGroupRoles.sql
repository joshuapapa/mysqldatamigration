
SET SQL_SAFE_UPDATES = 0;

Update `webscale-platform-workforce`.user_group_roles
Set is_primary_account = NULL
WHERE is_primary_account = 0;

Update `webscale-platform-workforce`.user_group_roles
Set is_shared_to_account_groups = NULL
WHERE is_shared_to_account_groups = 0;


SET SQL_SAFE_UPDATES = 1;