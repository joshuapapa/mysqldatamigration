Use `webscale-platform-workforce`;

INSERT INTO `webscale-platform-workforce`.multiplier_configuration
SELECT unhex(replace(uuid(), '-', '')), g.id, 
'2018-12-16 00:00:00', 0,0,0,now(),now(),null,null,'0,0,0,0,0,0,0,0,0.2,0.6,0.5,0.4,0.2,0.2,0.3,0.3,0.2,0.4,0,0,0,0,0,0',1
FROM `webscale-platform-workforce`.groups g
WHERE g.is_account_type = 0 and g.active = 1;