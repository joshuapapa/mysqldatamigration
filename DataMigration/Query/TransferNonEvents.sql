USE `webscale-platform-workforce`;

call DataMigrationSourceSkills;

call DataMigrationSameSchema;
call DataMigrationSameSchemaTest;

call DataMigrationUsers;
call DataMigrationUsersTest;

call DataMigrationAddedAccount;
call DataMigrationAddedAccountTest;

call DataMigrationUpdateEvent;
call DataMigrationEventTest;

call DataMigrationGroup;
call DataMigrationGroupTest;

call DataMigrationUserGroupRoles;
call DataMigrationUserGroupRolesAccount;
call DataMigrationUserGroupRolesTest;

call DataMigrationSLA;
call DataMigrationSLATest;

call DataMigrationACNProfile;
call DataMigrationACNProfileTest;

call DataMigrationUserSkills;

call DataMigrationAccountGroupSkill;
call DataMigrationAccountGroupSkillTest;

call DataMigrationGroupWorkloads;
call DataMigrationGroupWorkloadsTest;

call DataMigrationHeadcountForecast;
call DataMigrationHeadcountForecastTest;