USE `webscale-platform-workforce`;

call DataMigrationSameSchemaTest;
call DataMigrationUsersTest;
call DataMigrationAddedAccountTest;
call DataMigrationEventTest;
call DataMigrationGroupTest;
call DataMigrationUserGroupRolesTest;
call DataMigrationSLATest;
call DataMigrationACNProfileTest;
call DataMigrationAccountGroupSkillTest;
call DataMigrationGroupWorkloadsTest;
call DataMigrationHeadcountForecastTest;