Use `webscale-platform-workforce`;

DROP PROCEDURE IF EXISTS DataMigrationTransferEvent;
DROP PROCEDURE IF EXISTS DataMigrationSourceSkills;

DROP PROCEDURE IF EXISTS DataMigrationSameSchema;
DROP PROCEDURE IF EXISTS DataMigrationSameSchemaTest;

DROP PROCEDURE IF EXISTS DataMigrationUsers;
DROP PROCEDURE IF EXISTS DataMigrationUsersTest;

DROP PROCEDURE IF EXISTS DataMigrationAddedAccount;
DROP PROCEDURE IF EXISTS DataMigrationAddedAccountTest;

DROP PROCEDURE IF EXISTS DataMigrationUpdateEvent;
DROP PROCEDURE IF EXISTS DataMigrationEventTest;

DROP PROCEDURE IF EXISTS DataMigrationGroup;
DROP PROCEDURE IF EXISTS DataMigrationGroupTest;

DROP PROCEDURE IF EXISTS DataMigrationUserGroupRoles;
DROP PROCEDURE IF EXISTS DataMigrationUserGroupRolesAccount;
DROP PROCEDURE IF EXISTS DataMigrationUserGroupRolesTest;

DROP PROCEDURE IF EXISTS DataMigrationSLA;
DROP PROCEDURE IF EXISTS DataMigrationSLATest;

DROP PROCEDURE IF EXISTS DataMigrationACNProfile;
DROP PROCEDURE IF EXISTS DataMigrationACNProfileTest;

DROP PROCEDURE IF EXISTS DataMigrationUserSkills;

DROP PROCEDURE IF EXISTS DataMigrationAccountGroupSkill;
DROP PROCEDURE IF EXISTS DataMigrationAccountGroupSkillTest;

DROP PROCEDURE IF EXISTS DataMigrationGroupWorkloads;
DROP PROCEDURE IF EXISTS DataMigrationGroupWorkloadsTest;

DROP PROCEDURE IF EXISTS DataMigrationHeadcountForecast;
DROP PROCEDURE IF EXISTS DataMigrationHeadcountForecastTest;