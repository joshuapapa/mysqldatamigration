DELETE FROM `webscale-platform-workforce`.events
WHERE active in (1,0);

SET @data_limit = 20000;

call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);

SELECT 200000 as 'Max Possible Data';

call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);

SELECT 400000 as 'Max Possible Data';

call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);

SELECT 600000 as 'Max Possible Data';

call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);
call DataMigrationTransferEvent(@data_limit);

SELECT 800000 as 'Max Possible Data';