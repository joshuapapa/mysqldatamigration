
/** USER GROUP ROLE MIGRATION SCRIPT **/

#CALL `webscale-platform-workforce`.`DataMigrationUserGroupRolesAccount`();


DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationUserGroupRolesAccount $$

CREATE PROCEDURE DataMigrationUserGroupRolesAccount()

BEGIN
    
	SET FOREIGN_KEY_CHECKS = 0;
	SET SQL_SAFE_UPDATES = 0;

	INSERT INTO `webscale-platform-workforce`.user_group_roles (
		id
		, user_id
		, group_id
		, role_id
		, is_member
		, active
		, inserted_at
		, updated_at
		, creation_user_id
		, update_user_id
		, is_primary_account
		, is_group_dedicated
		, is_shared_to_account_groups
		, has_org_access
		, availability_start_date
		, availability_end_date
		, production_start_date
		, production_end_date
	)
	SELECT 
		UNHEX(REPLACE(UUID(), "-",""))
		, ur.user_id
		, (SELECT id FROM `webscale-platform-workforce`.groups WHERE short_name = 'seattle' and active = 1) /*group_id*/
		, CASE
			WHEN ur.role_id = 3 OR (ur.role_id = 2 AND (SELECT DISTINCT user_id FROM `webscale-platform-workforce`.user_group_roles WHERE ur.user_id = user_id) IS NULL)
			THEN 4
			ELSE ur.role_id
		  END /*role_id*/
		, 1 /*is_member*/
		, (SELECT active FROM `webscale-platform-workforce`.users WHERE id = ur.user_id) /*active*/
		, (SELECT inserted_at FROM `webscale-platform-seattle`.users WHERE id = ur.user_id) /*inserted_at*/
		, now() /*updated_at*/
		, NULL /*creation_user_id*/
		, NULL /*update_user_id*/
		, 1 /*is_primary_account*/
		, 0 /*is_group_dedicated*/
		, 0 /*is_shared_to_account_groups*/
		, CASE
			WHEN ur.role_id = 1
            THEN 1
			ELSE 0
		  END /******has_org_access*/
		, NULL /*availability_start_date*/
		, NULL /*availability_end_date*/
		, NULL /*production_start_date*/
		, NULL /*production_end_date*/
	FROM `webscale-platform-workforce`.user_roles_temp ur;
    
	SET FOREIGN_KEY_CHECKS = 1;
	SET SQL_SAFE_UPDATES = 1;

END $$

DELIMITER ;


