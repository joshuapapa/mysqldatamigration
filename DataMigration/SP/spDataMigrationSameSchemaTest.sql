Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationSameSchemaTest $$

CREATE PROCEDURE DataMigrationSameSchemaTest()
BEGIN

DECLARE test_column_agent_workloads INT;
DECLARE test_column_audit_logs INT;
DECLARE test_column_consent_records INT;
DECLARE test_column_consumer INT;
DECLARE test_column_data_retention INT;
DECLARE test_column_eventtypes INT;
DECLARE test_column_file_storage INT;
DECLARE test_column_monitoring INT;
DECLARE test_column_monitoring_configuration INT;
DECLARE test_column_multiplier_configuration INT;
DECLARE test_column_other_roles INT;
DECLARE test_column_reference_data INT;
DECLARE test_column_reference_data_types INT;
DECLARE test_column_shift_rules INT;
DECLARE test_column_shift_template INT;
DECLARE test_column_sla_snapshot INT;
DECLARE test_column_tasks INT;
DECLARE test_column_uploaded_files INT;
DECLARE test_column_volume_sets INT;
DECLARE test_column_volumes INT;

DECLARE test_count_agent_workloads INT;
DECLARE test_count_audit_logs INT;
DECLARE test_count_consent_records INT;
DECLARE test_count_consumer INT;
DECLARE test_count_data_retention INT;
DECLARE test_count_eventtypes INT;
DECLARE test_count_file_storage INT;
DECLARE test_count_monitoring INT;
DECLARE test_count_monitoring_configuration INT;
DECLARE test_count_multiplier_configuration INT;
DECLARE test_count_other_roles INT;
DECLARE test_count_reference_data INT;
DECLARE test_count_reference_data_types INT;
DECLARE test_count_shift_rules INT;
DECLARE test_count_shift_template INT;
DECLARE test_count_sla_snapshot INT;
DECLARE test_count_tasks INT;
DECLARE test_count_uploaded_files INT;
DECLARE test_count_volume_sets INT;
DECLARE test_count_volumes INT;

DECLARE overall_flag BIT;

DROP TEMPORARY TABLE IF EXISTS TestResult;
CREATE TEMPORARY TABLE TestResult (id INT NOT NULL AUTO_INCREMENT, test_case VARCHAR(255), result VARCHAR(255), group_id BINARY(16), PRIMARY KEY (id));

SET overall_flag = 1;

SET test_column_agent_workloads = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='agent_workloads' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='agent_workloads' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_agent_workloads = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.agent_workloads) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.agent_workloads) as tb2);

SET test_column_audit_logs = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='audit_logs' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='audit_logs' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_audit_logs = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.audit_logs) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.audit_logs) as tb2);

SET test_column_consent_records = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='consent_records' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='consent_records' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_consent_records = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.consent_records) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.consent_records) as tb2);

SET test_column_consumer = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='consumer' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='consumer' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_consumer = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.consumer) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.consumer) as tb2);

SET test_column_data_retention = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='data_retention' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='data_retention' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_data_retention = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.data_retention) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.data_retention) as tb2);

SET test_column_eventtypes = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='eventtypes' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='eventtypes' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_eventtypes = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.eventtypes) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.eventtypes) as tb2);

SET test_column_file_storage  = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='file_storage' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='file_storage' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_file_storage = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.file_storage) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.file_storage) as tb2);

SET test_column_monitoring  = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='monitoring' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='monitoring' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_monitoring = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.monitoring) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.monitoring) as tb2);

SET test_column_monitoring_configuration  = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='monitoring_configuration' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='monitoring_configuration' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_monitoring_configuration = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.monitoring_configuration) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.monitoring_configuration) as tb2);

SET test_column_multiplier_configuration  = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='multiplier_configuration' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='multiplier_configuration' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

#BUG FIX UPDATE
SET test_count_multiplier_configuration = (SELECT tb1.cnt + 1 = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.multiplier_configuration) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.multiplier_configuration) as tb2);

SET test_column_other_roles  = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='other_roles' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='other_roles' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_other_roles = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.other_roles) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.other_roles) as tb2);

SET test_column_reference_data  = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='reference_data' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='reference_data' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_reference_data = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.reference_data) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.reference_data) as tb2);

SET test_column_reference_data_types  = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='reference_data_types' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='reference_data_types' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_reference_data_types = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.reference_data_types) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.reference_data_types) as tb2);

SET test_column_shift_rules  = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='shift_rules' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='shift_rules' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_shift_rules = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.shift_rules) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.shift_rules) as tb2);

SET test_column_shift_template  = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='shift_template' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='shift_template' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_shift_template = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.shift_template) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.shift_template) as tb2);

SET test_column_sla_snapshot  = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='sla_snapshot' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='sla_snapshot' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_sla_snapshot = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.sla_snapshot) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.sla_snapshot) as tb2);

SET test_column_tasks = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='tasks' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='tasks' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_tasks = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.tasks) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.tasks) as tb2);

SET test_column_uploaded_files  = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='uploaded_files' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='uploaded_files' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_uploaded_files = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.uploaded_files) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.uploaded_files) as tb2);

SET test_column_volume_sets  = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='volume_sets' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='volume_sets' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_volume_sets  = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.volume_sets) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.volume_sets) as tb2);

SET test_column_volumes  = (SELECT count(*) FROM information_schema.columns as info1  
WHERE table_schema='webscale-platform-workforce' AND table_name='volumes' AND NOT EXISTS(
	SELECT COLUMN_NAME as col FROM information_schema.columns as info2
	WHERE table_schema='webscale-platform-seattle' AND table_name='volumes' AND info1.COLUMN_NAME = info2.COLUMN_NAME
));

SET test_count_volumes  = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.volumes) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.volumes) as tb2);

#TEST COLUMN AGENT WORKLOADS
IF test_column_agent_workloads = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN AGENT WORKLOADS", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN AGENT WORKLOADS", "FAILED", null);
END IF;

#TEST COUNT AGENT WORKLOADS
IF test_count_agent_workloads = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT AGENT WORKLOADS", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT AGENT WORKLOADS", "FAILED", null);
END IF;

#TEST COLUMN AUDIT LOGS
IF test_column_audit_logs = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN AUDIT LOGS", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN AUDIT LOGS", "FAILED", null);
END IF;

#TEST COUNT AUDIT LOGS
IF test_count_audit_logs = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT AUDIT LOGS", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT AUDIT LOGS", "FAILED", null);
END IF;

#TEST COLUMN CONSENT RECORDS
IF test_column_consent_records = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN CONSENT RECORDS", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN CONSENT RECORDS", "FAILED", null);
END IF;

#TEST COUNT CONSENT RECORDS
IF test_count_consent_records = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT CONSENT RECORDS", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT CONSENT RECORDS", "FAILED", null);
END IF;

#TEST COLUMN CONSUMER
IF test_column_consumer = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN CONSUMER", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN CONSUMER", "FAILED", null);
END IF;

#TEST COUNT CONSUMER
IF test_count_consumer = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT CONSUMER", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT CONSUMER", "FAILED", null);
END IF;

#TEST COLUMN DATA RETENTION
IF test_column_data_retention = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN DATA RETENTION", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN DATA RETENTION", "FAILED", null);
END IF;

#TEST COUNT DATA RETENTION
IF test_count_data_retention = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT DATA RETENTION", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT DATA RETENTION", "FAILED", null);
END IF;

#TEST COLUMN EVENT TYPES
IF test_column_eventtypes = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN EVENT TYPES", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN EVENT TYPES", "FAILED", null);
END IF;

#TEST COUNT EVENT TYPES
IF test_count_eventtypes = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT EVENT TYPES", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT EVENT TYPES", "FAILED", null);
END IF;

#TEST COLUMN FILE STORAGE
IF test_column_file_storage = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN FILE STORAGE", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN FILE STORAGE", "FAILED", null);
END IF;

#TEST COUNT FILE STORAGE
IF test_count_file_storage = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT FILE STORAGE", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT FILE STORAGE", "FAILED", null);
END IF;

#TEST COLUMN MONITORING
IF test_column_monitoring  = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN MONITORING", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN MONITORING", "FAILED", null);
END IF;

#TEST COUNT MONITORING
IF test_count_monitoring  = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT MONITORING", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT MONITORING", "FAILED", null);
END IF;

#TEST COLUMN MONITORING CONFIGURATION
IF test_column_monitoring_configuration   = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN MONITORING CONFIGURATION", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN MONITORING CONFIGURATION", "FAILED", null);
END IF;

#TEST COUNT MONITORING CONFIGURATION
IF test_count_monitoring_configuration   = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT MONITORING CONFIGURATION", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT MONITORING CONFIGURATION", "FAILED", null);
END IF;

#TEST COLUMN MULTIPLIER CONFIGURATION
IF test_column_multiplier_configuration    = 2 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN MULTIPLIER CONFIGURATION", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN MULTIPLIER CONFIGURATION", "FAILED", null);
END IF;

#TEST COUNT MULTIPLIER CONFIGURATION
IF test_count_multiplier_configuration    = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT MULTIPLIER CONFIGURATION", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT MULTIPLIER CONFIGURATION", "FAILED", null);
END IF;

#TEST COLUMN OTHER ROLES
IF test_column_other_roles = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN OTHER ROLES", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN OTHER ROLES", "FAILED", null);
END IF;

#TEST COUNT OTHER ROLES
IF test_count_other_roles = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT OTHER ROLES", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT OTHER ROLES", "FAILED", null);
END IF;

#TEST COLUMN REFERENCE DATA
IF test_column_reference_data = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN REFERENCE DATA", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN REFERENCE DATA", "FAILED", null);
END IF;

#TEST COUNT REFERENCE DATA
IF test_count_reference_data = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT REFERENCE DATA", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT REFERENCE DATA", "FAILED", null);
END IF;

#TEST COLUMN REFERENCE DATA TYPES
IF test_column_reference_data_types  = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN REFERENCE DATA TYPES", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN REFERENCE DATA TYPES", "FAILED", null);
END IF;

#TEST COUNT REFERENCE DATA TYPES
IF test_count_reference_data_types  = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT REFERENCE DATA TYPES", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT REFERENCE DATA TYPES", "FAILED", null);
END IF;

#TEST COLUMN SHIFT RULES
IF test_column_shift_rules   = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN SHIFT RULES", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN SHIFT RULES", "FAILED", null);
END IF;

#TEST COUNT SHIFT RULES
IF test_count_shift_rules = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT SHIFT RULES", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT SHIFT RULES", "FAILED", null);
END IF;

#TEST COLUMN SHIFT TEMPLATE
IF test_column_shift_template   = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN SHIFT TEMPLATE", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN SHIFT TEMPLATE", "FAILED", null);
END IF;

#TEST COUNT SHIFT TEMPLATE
IF test_count_shift_template   = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT SHIFT TEMPLATE", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT SHIFT TEMPLATE", "FAILED", null);
END IF;

#TEST COLUMN SLA SNAPSHOT
IF test_column_sla_snapshot   = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN SLA SNAPSHOT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN SLA SNAPSHOT", "FAILED", null);
END IF;

#TEST COUNT SLA SNAPSHOT
IF test_count_sla_snapshot   = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT SLA SNAPSHOT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT SLA SNAPSHOT", "FAILED", null);
END IF;

#TEST COLUMN TASKS
IF test_column_tasks = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN TASKS", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN TASKS", "FAILED", null);
END IF;

#TEST COUNT TASKS
IF test_count_tasks = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT TASKS", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT TASKS", "FAILED", null);
END IF;

#TEST COLUMN UPLOADED FILES
IF test_column_uploaded_files = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN UPLOADED FILES", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN UPLOADED FILES", "FAILED", null);
END IF;

#TEST COUNT UPLOADED FILES
IF test_count_uploaded_files = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT UPLOADED FILES", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT UPLOADED FILES", "FAILED", null);
END IF;

#TEST COLUMN VOLUME SETS
IF test_column_volume_sets  = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN VOLUME SETS", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN VOLUME SETS", "FAILED", null);
END IF;

#TEST COUNT VOLUME SETS
IF test_count_volume_sets  = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT VOLUME SETS", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT VOLUME SETS", "FAILED", null);
END IF;

#TEST COLUMN VOLUME
IF test_column_volumes   = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN VOLUME", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COLUMN VOLUME", "FAILED", null);
END IF;

#TEST COUNT VOLUME
IF test_count_volumes   = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT VOLUME", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT VOLUME", "FAILED", null);
END IF;

#TEST OVERALL
if overall_flag = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "PASSED", null);
ELSE 
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "FAILED", null);
END IF;

#DISPLAY RESULT
SELECT id as 'ID', test_case as 'TEST CASE', result as 'RESULT', hex(group_id) as 'GROUP ID' FROM TestResult;

DROP TEMPORARY TABLE TestResult;
END $$

DELIMITER ;