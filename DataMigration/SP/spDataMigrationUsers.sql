
/** MIGRATE USERS SCRIPT **/

#CALL `webscale-platform-workforce`.`DataMigrationUsers`();


DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationUsers $$

CREATE PROCEDURE DataMigrationUsers()

BEGIN
	
	DECLARE sourceDB VARCHAR(35);
    DECLARE destinationDB varchar(35);
    
	SET FOREIGN_KEY_CHECKS = 0;
    SET SQL_SAFE_UPDATES = 0;
    
    TRUNCATE `webscale-platform-workforce`.users;
    
	INSERT INTO `webscale-platform-workforce`.users (
		id
		, email_address
		, username
		, password
		, active
		, long_name
		, description
		, creation_user_id
		, update_user_id
		, inserted_at
		, updated_at
		, resource_type
		, eligible_holidays
		, carried_over_holidays
		, timezone
		, acn_enterprise_id
		, office_phone
		, sap_id
		, contractor_client_id
		, supervisor_eid
		, career_level
		, job_roles_id
		, seat_location
		, training_end_date
		, profile_status_id
		, primary_group_id
		, preferred_name
		, other_emails
		, organization_unit_id
		, contractor_client_name_id
		, wave_name
		, production_start_date
		, production_end_date
		, profile_url
		, locale_id
		, primary_account_id
		, acn_profile_id
		, is_share_to_account
		, iw_id
	)
	SELECT
		id
		, email_address
		, username
		, password
		, active
		, long_name
		, description
		, creation_user_id
		, update_user_id
		, inserted_at
		, updated_at
		, resource_type
		, eligible_holidays
		, carried_over_holidays
		, timezone
		, acn_enterprise_id
		, office_phone
		, sap_id
		, contractor_client_id
		, supervisor_eid
		, career_level
		, job_roles_id
		, seat_location
		, training_end_date
		, profile_status_id
		, primary_group_id
		, preferred_name
		, other_emails
		, organization_unit_id
		, contractor_client_name_id
		, wave_name
		, production_start_date
		, production_end_date
		, profile_url
		, locale_id
		, (SELECT id FROM `webscale-platform-workforce`.groups WHERE short_name = 'seattle')
		, NULL
		, 0
		, iw_id
	FROM `webscale-platform-seattle`.users su
	WHERE NOT EXISTS (
		SELECT id FROM `webscale-platform-workforce`.users WHERE id = su.id
	);
	
	SET FOREIGN_KEY_CHECKS = 1;
    SET SQL_SAFE_UPDATES = 1;

END $$

DELIMITER ;