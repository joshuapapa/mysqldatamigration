Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationGroupTest $$

CREATE PROCEDURE DataMigrationGroupTest()
BEGIN

DECLARE test_location_count INT;
DECLARE test_old_group_count INT;
DECLARE test_old_active_group_count INT;
DECLARE test_new_group_count INT;
DECLARE test_account_count INT;
DECLARE test_grouphist_count INT;
DECLARE test_accounthist_count INT;
DECLARE overall_flag BIT;
DECLARE seattle_account_id BINARY(16);

DROP TEMPORARY TABLE IF EXISTS TestResult;
CREATE TEMPORARY TABLE TestResult (id INT NOT NULL AUTO_INCREMENT, test_case VARCHAR(255), result VARCHAR(255), group_id BINARY(16), PRIMARY KEY (id));

SET overall_flag = 1;
SET seattle_account_id = 0xe54dec54ebdb11e888e91860244727eb;
SET test_old_group_count = (Select count(*) from `webscale-platform-seattle`.groups);
SET test_old_active_group_count = (Select count(*) from `webscale-platform-seattle`.groups where groups.active = 1);
SET test_new_group_count = (Select count(*) from `webscale-platform-workforce`.groups where groups.is_account_type = 0 and groups.account_group_id = seattle_account_id);
SET test_account_count = (Select count(*) from `webscale-platform-workforce`.groups where groups.is_account_type = 1 and groups.id = seattle_account_id);
SET test_location_count = (Select count(*) from `webscale-platform-workforce`.groups where geographic_region is null);
SET test_grouphist_count = (Select count(*) from `webscale-platform-workforce`.groups_hist where groups_hist.account_group_id = seattle_account_id);
SET test_accounthist_count = (Select count(*) from `webscale-platform-workforce`.groups_hist where groups_hist.group_id = seattle_account_id);

#TEST GROUP COUNT
IF test_old_group_count = test_new_group_count THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST GROUP COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST GROUP COUNT", "FAILED", null);
END IF;

#TEST GROUP HISTORY COUNT
IF test_grouphist_count = test_old_active_group_count THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST GROUP HISTORY COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST GROUP HISTORY COUNT", "FAILED", null);
END IF;

#TEST ACCOUNT COUNT
IF test_account_count = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST ACCOUNT COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST ACCOUNT COUNT", "FAILED", null);
END IF;

#TEST ACCOUNT HISTORY COUNT
IF test_accounthist_count = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST ACCOUNT HISTORY COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST ACCOUNT HISTORY COUNT", "FAILED", null);
END IF;

#TEST MISSING LOCATION
IF test_location_count = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST MISSING LOCATION", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id)
	SELECT "TEST MISSING LOCATION", "FAILED", g.id from `webscale-platform-workforce`.groups as g where geographic_region is null;
END IF;

#TEST OVERALL
if overall_flag = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "PASSED", null);
ELSE 
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "FAILED", null);
END IF;

#DISPLAY RESULT
SELECT id as 'ID', test_case as 'TEST CASE', result as 'RESULT', hex(group_id) as 'GROUP ID' FROM TestResult;

DROP TEMPORARY TABLE TestResult;
END $$

DELIMITER ;