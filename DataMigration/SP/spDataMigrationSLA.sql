Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationSLA $$

CREATE PROCEDURE DataMigrationSLA()
BEGIN
SET FOREIGN_KEY_CHECKS=0;
SET SQL_SAFE_UPDATES=0;

DELETE FROM `webscale-platform-workforce`.sla;
#Insert SLA
INSERT INTO `webscale-platform-workforce`.sla(id,group_id,task_id,sla_snapshot_id,sla,inserted_at,updated_at,creation_user_id,update_user_id,source_skills_id)
SELECT s.id,s.group_id,s.task_id,s.sla_snapshot_id,s.sla,s.inserted_at,s.updated_at,s.creation_user_id,s.update_user_id, null
FROM `webscale-platform-seattle`.sla s;

#Update SLA source skill
UPDATE `webscale-platform-workforce`.sla s1
SET s1.source_skills_id = (SELECT id from source_skills where name = 'Azerbaijani' LIMIT 1)
WHERE s1.id IN (Select s2.id 
			FROM `webscale-platform-seattle`.sla s2
			JOIN `webscale-platform-seattle`.skillsbases sk1
			ON s2.skill_base_id = sk1.id
			WHERE sk1.value = 'Azerbaijani');

SET FOREIGN_KEY_CHECKS=1;
SET SQL_SAFE_UPDATES=0;
END $$
DELIMITER ;