
/** USER GROUP ROLE MIGRATION SCRIPT **/

#CALL `webscale-platform-workforce`.`DataMigrationUserGroupRoles`();


DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationUserGroupRoles $$

CREATE PROCEDURE DataMigrationUserGroupRoles()

BEGIN
    
	SET FOREIGN_KEY_CHECKS = 0;
	SET SQL_SAFE_UPDATES = 0;
    
    BEGIN
		TRUNCATE `webscale-platform-workforce`.user_group_roles;
        
        DROP TABLE IF EXISTS `webscale-platform-workforce`.user_roles_temp;
		CREATE TEMPORARY TABLE IF NOT EXISTS `webscale-platform-workforce`.user_roles_temp AS (SELECT * FROM `webscale-platform-seattle`.user_roles);
        
        DROP TABLE IF EXISTS `webscale-platform-workforce`.membership_temp;
		CREATE TEMPORARY TABLE IF NOT EXISTS `webscale-platform-workforce`.membership_temp AS (SELECT * FROM `webscale-platform-seattle`.membership);
	END;
    
    
    BEGIN
		DELETE ur1 FROM `webscale-platform-workforce`.user_roles_temp ur1
		INNER JOIN `webscale-platform-seattle`.user_roles ur2
		WHERE ur1.user_id = ur2.user_id AND ur1.role_id > ur2.role_id;
        
        DELETE m1 FROM `webscale-platform-workforce`.membership_temp m1
		INNER JOIN `webscale-platform-seattle`.membership m2
		WHERE m1.user_id = m2.user_id
        AND m1.child_group_id = m2.child_group_id
        AND m1.membership_type_id != m2.membership_type_id
        AND m2.membership_type_id = (SELECT id FROM `webscale-platform-seattle`.membership_types WHERE name = 'REGULAR');
    END;

	INSERT INTO `webscale-platform-workforce`.user_group_roles (
		id
		, user_id
		, group_id
		, role_id
		, is_member
		, active
		, inserted_at
		, updated_at
		, creation_user_id
		, update_user_id
		, is_primary_account
		, is_group_dedicated
		, is_shared_to_account_groups
		, has_org_access
		, availability_start_date
		, availability_end_date
		, production_start_date
		, production_end_date
	)
	SELECT 
		UNHEX(REPLACE(UUID(), "-",""))
		, ug.user_id
		, CASE 
			WHEN ug.child_group_id IS NULL
			THEN (SELECT id FROM `webscale-platform-workforce`.groups WHERE short_name = 'seattle' and active = 1)
			ELSE ug.child_group_id
		  END /*group_id*/
		, CASE
			WHEN ug.role_id = 3
				OR (ug.role_id = 2 AND ug.child_group_id IS NULL)
				OR ug.role_id = 1
			THEN 4
			ELSE ug.role_id
		  END /*role_id*/
		, 1 /*is_member*/
		, (SELECT active FROM `webscale-platform-workforce`.users WHERE id = ug.user_id) /*active*/
		, (SELECT inserted_at FROM `webscale-platform-seattle`.users WHERE id = ug.user_id) /*inserted_at*/
		, now() /*updated_at*/
		, NULL /*creation_user_id*/
		, NULL /*update_user_id*/
		, NULL /*is_primary_account*/
		, 0 /*is_group_dedicated*/
		, NULL /*is_shared_to_account_groups*/
		, CASE
			WHEN ug.role_id = 2 AND ug.child_group_id IS NOT NULL
            THEN 1
			ELSE 0
		END /*has_org_access*/
		, NULL /*availability_start_date*/
		, NULL /*availability_end_date*/
		, NULL /*production_start_date*/
		, NULL /*production_end_date*/
	FROM (
		SELECT
			ur.user_id
			, ur.role_id
			, m.child_group_id
			, mt.name
		FROM `webscale-platform-workforce`.membership_temp m
		INNER JOIN `webscale-platform-workforce`.user_roles_temp ur
			ON ur.user_id = m.user_id
		INNER JOIN `webscale-platform-seattle`.membership_types mt
			ON m.membership_type_id = mt.id
	) AS ug;
	
	SET FOREIGN_KEY_CHECKS = 1;
	SET SQL_SAFE_UPDATES = 1;

END $$

DELIMITER ;


