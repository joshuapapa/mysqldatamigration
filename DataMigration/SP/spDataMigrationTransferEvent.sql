Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationTransferEvent $$

CREATE PROCEDURE DataMigrationTransferEvent(data_limit INT) 

BEGIN

SET FOREIGN_KEY_CHECKS=0;

#Transer Events

INSERT INTO `webscale-platform-workforce`.events(id,date_start, date_end,state,billable,event_desc,user_id,event_type_id,inserted_at,updated_at,reason_code,shift_template_id,net_duration,task_id,group_id,approved_at,approve_user_id,creation_user_id,update_user_id,link_id,active,notes)
Select id,date_start, date_end,state,billable,event_desc,user_id,event_type_id,inserted_at,updated_at,reason_code,shift_template_id,net_duration,task_id,group_id,approved_at,approve_user_id,creation_user_id,update_user_id,link_id,active,notes
FROM `webscale-platform-seattle`.events 
WHERE id NOT IN(SELECT id FROM `webscale-platform-workforce`.events)
LIMIT data_limit;

SET FOREIGN_KEY_CHECKS=1;

END $$

DELIMITER ;