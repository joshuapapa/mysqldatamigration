Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationGroupWorkloads $$

CREATE PROCEDURE DataMigrationGroupWorkloads()
BEGIN
SET FOREIGN_KEY_CHECKS=0;
SET SQL_SAFE_UPDATES=0;

DELETE FROM `webscale-platform-workforce`.group_workloads;

#Insert Group Workloads
INSERT INTO `webscale-platform-workforce`.group_workloads(id,group_id,task_id,quantity,inserted_at,updated_at,creation_user_id,update_user_id)
SELECT id,group_id,task_id,quantity,inserted_at,updated_at,creation_user_id,update_user_id
FROM `webscale-platform-seattle`.group_workloads;

#Update Group Workloads Skill
UPDATE `webscale-platform-workforce`.group_workloads gw1
SET gw1.source_skills_id = (SELECT id from source_skills where name = 'Azerbaijani' LIMIT 1)
WHERE gw1.id IN (Select gw2.id 
			FROM `webscale-platform-seattle`.group_workloads gw2
			JOIN `webscale-platform-seattle`.skillsbases sk1
			ON gw2.skill_base_id = sk1.id
			WHERE sk1.value = 'Azerbaijani');

SET FOREIGN_KEY_CHECKS=1;
SET SQL_SAFE_UPDATES=0;
END $$
DELIMITER ;