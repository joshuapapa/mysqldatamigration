Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationGroup $$

CREATE PROCEDURE DataMigrationGroup()
BEGIN

DECLARE seattle_account_id BINARY(16);

SET FOREIGN_KEY_CHECKS=0;
SET SQL_SAFE_UPDATES=0;
SET seattle_account_id = 0xe54dec54ebdb11e888e91860244727eb;

DELETE FROM `webscale-platform-workforce`.groups
WHERE active in (1, 0);

DELETE FROM `webscale-platform-workforce`.groups_hist
WHERE active in (1, 0);

#Insert Account
INSERT INTO `webscale-platform-workforce`.groups(id,short_name,long_name,description,creation_user_id,update_user_id,inserted_at,updated_at,parent_group_id,active,timezone,is_account_type, account_group_id, config_import, is_volume)
VALUES(seattle_account_id, 'seattle', 'Seattle', 'Microsoft', null, null, now(), now(), null, 1, 'Asia/Manila', 1,null,1, 1);

#Insert Account History
INSERT INTO `webscale-platform-workforce`.groups_hist(id,group_id,parent_group_id,active,inserted_at,updated_at,creation_user_id,update_user_id,account_group_id,for_distribution, is_hourly_multiplier)
VALUES(unhex(replace(uuid(), '-', '')), seattle_account_id, null, 1, now(), now(), null, null, null, 0, 0);

#Insert Group
INSERT INTO `webscale-platform-workforce`.groups(id,short_name,long_name,description,creation_user_id,update_user_id,inserted_at,updated_at,parent_group_id,active,timezone,is_account_type, account_group_id, config_import, is_volume)
SELECT g.id, g.short_name, g.long_name, g.description, g.creation_user_id, g.update_user_id, g.inserted_at, g.updated_at, g.parent_group_id, g.active, g.timezone, 0, seattle_account_id, null, null
FROM `webscale-platform-seattle`.groups as g;

#Insert Group History
INSERT INTO `webscale-platform-workforce`.groups_hist(id,group_id,parent_group_id,active,inserted_at,updated_at,creation_user_id,update_user_id,account_group_id,for_distribution, is_hourly_multiplier)
SELECT unhex(replace(uuid(), '-', '')), g.id, g.parent_group_id, g.active, g.inserted_at, g.updated_at, g.creation_user_id, g.update_user_id, seattle_account_id, null,0
FROM `webscale-platform-seattle`.groups as g
where g.active = 1;

UPDATE `webscale-platform-workforce`.groups_hist as gh
SET gh.is_hourly_multiplier = 1 
WHERE  gh.group_id = seattle_account_id;

#Update Account Location
Update `webscale-platform-workforce`.groups as g1
Set g1.geographic_region = 'AAPAC'
where g1.id = seattle_account_id;

#Update Group Location
Update `webscale-platform-workforce`.groups as g1
Set g1.geographic_region = 'AAPAC', g1.geographic_unit = 'ASEAN', g1.country = 'Malaysia', g1.metro_city = 'Kuala Lumpur', g1.city = 'Kuala Lumpur'
where g1.id in (Select g2.id 
	from `webscale-platform-seattle`.groups as g2
    JOIN `webscale-platform-seattle`.locations as l1
    ON g2.location_id = l1.id
    where l1.long_name = 'Kuala Lumpur'
);

Update `webscale-platform-workforce`.groups as g1
Set g1.geographic_region = 'AAPAC', g1.geographic_unit = 'ASEAN', g1.country = 'Philippines', g1.metro_city = 'ManilaCity', g1.city = 'Quezon', g1.facility = 'Quezon Global One Center'
where g1.id in (Select g2.id 
	from `webscale-platform-seattle`.groups as g2
    JOIN `webscale-platform-seattle`.locations as l1
    ON g2.location_id = l1.id
    where l1.long_name = 'Global One'
);

Update `webscale-platform-workforce`.groups as g1
Set g1.geographic_region = 'Europe', g1.geographic_unit = 'ICEG', g1.country = 'Slovakia', g1.metro_city = 'Bratislava', g1.city = 'Bratislava'
where g1.id in (Select g2.id 
	from `webscale-platform-seattle`.groups as g2
    JOIN `webscale-platform-seattle`.locations as l1
    ON g2.location_id = l1.id
    where l1.long_name = 'Bratislava'
);

Update `webscale-platform-workforce`.groups as g1
Set g1.geographic_region = 'AAPAC', g1.geographic_unit = 'ASEAN', g1.country = 'Philippines'
where g1.id in (Select g2.id 
	from `webscale-platform-seattle`.groups as g2
    JOIN `webscale-platform-seattle`.locations as l1
    ON g2.location_id = l1.id
    where l1.long_name = 'PH'
);


Update `webscale-platform-workforce`.groups as g1
Set g1.geographic_region = 'AAPAC', g1.geographic_unit = 'ASEAN', g1.country = 'Philippines', g1.metro_city = 'ManilaCity', g1.city = 'Taguig', g1.facility = 'Taguig Uptown Bonifacio Tower 3'
where g1.id in (Select g2.id 
	from `webscale-platform-seattle`.groups as g2
    JOIN `webscale-platform-seattle`.locations as l1
    ON g2.location_id = l1.id
    where l1.long_name = 'Uptown 3'
);

Update `webscale-platform-workforce`.groups as g1
Set g1.geographic_region = 'AAPAC', g1.geographic_unit = 'ASEAN', g1.country = 'Philippines', g1.metro_city = 'CebuCity', g1.city = 'Cebu'
where g1.id in (Select g2.id 
	from `webscale-platform-seattle`.groups as g2
    JOIN `webscale-platform-seattle`.locations as l1
    ON g2.location_id = l1.id
    where l1.long_name = 'Cebu'
);

Update `webscale-platform-workforce`.groups as g1
Set g1.geographic_region = 'North America', g1.geographic_unit = 'United States', g1.country = 'USA'
where g1.id in (Select g2.id 
	from `webscale-platform-seattle`.groups as g2
    JOIN `webscale-platform-seattle`.locations as l1
    ON g2.location_id = l1.id
    where l1.long_name = 'Remote, WFH'
);

SET FOREIGN_KEY_CHECKS=1;
SET SQL_SAFE_UPDATES=1;

END $$

DELIMITER ;