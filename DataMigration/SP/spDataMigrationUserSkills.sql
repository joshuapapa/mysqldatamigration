
/** USER SKILLS MIGRATION SCRIPT **/

#CALL `webscale-platform-workforce`.`DataMigrationUserSkills`();


DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationUserSkills $$

CREATE PROCEDURE DataMigrationUserSkills()

BEGIN
    
	SET FOREIGN_KEY_CHECKS = 0;
    SET SQL_SAFE_UPDATES = 0;
    
    TRUNCATE `webscale-platform-workforce`.user_skills;

	INSERT INTO `webscale-platform-workforce`.user_skills(
		id
		, user_id
		, skill_id
		, proficiency
		, years_of_exp
		, last_year_used
		, inserted_at
		, updated_at
	)
	SELECT
		UNHEX(REPLACE(UUID(), "-",""))
		, s.user_id
		, s.id
		, NULL
		, 0
		, NULL
		, s.inserted_at
		, s.updated_at
	FROM (
		SELECT
			s.user_id
			, ss.id
            , ss.name
			, s.inserted_at
			, s.updated_at
		FROM `webscale-platform-seattle`.skills s
        INNER JOIN `webscale-platform-seattle`.skillsbases sb
			ON sb.id = s.skill_base_id
        INNER JOIN `webscale-platform-workforce`.source_skills ss
			ON ss.name = sb.value
			AND ss.source = 0
	) AS s;

	SET FOREIGN_KEY_CHECKS = 1;
    SET SQL_SAFE_UPDATES = 1;

END $$

DELIMITER ;
