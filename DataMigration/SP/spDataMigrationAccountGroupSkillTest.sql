Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationAccountGroupSkillTest $$

CREATE PROCEDURE DataMigrationAccountGroupSkillTest()
BEGIN

DECLARE account_group_skill_language_count INT;
DECLARE account_group_skill_custom_count INT;
DECLARE skill_language_count INT;
DECLARE overall_flag BIT;
DECLARE seattle_account_id BINARY(16);

DROP TEMPORARY TABLE IF EXISTS TestResult;
CREATE TEMPORARY TABLE TestResult (id INT NOT NULL AUTO_INCREMENT, test_case VARCHAR(255), result VARCHAR(255), group_id BINARY(16), PRIMARY KEY (id));

SET overall_flag = 1;
SET seattle_account_id = 0xe54dec54ebdb11e888e91860244727eb;

/*
SET account_group_skill_language_count = (SELECT count(ags.id) FROM `webscale-platform-workforce`.account_group_skills ags
										JOIN `webscale-platform-workforce`.source_skills ss 
                                        ON ags.skill_id = ss.id
                                        JOIN `webscale-platform-workforce`.skill_types st ON ss.skill_type_id = st.id
										WHERE st.code = '1');
*/                                      
SET account_group_skill_custom_count = (SELECT count(ags.id) FROM `webscale-platform-workforce`.account_group_skills ags
										JOIN `webscale-platform-workforce`.source_skills ss 
                                        ON ags.skill_id = ss.id
										JOIN `webscale-platform-workforce`.skill_types st ON ss.skill_type_id = st.id
										WHERE st.code != '1');
									/*
SET skill_language_count = (SELECT count(ss.id) FROM `webscale-platform-workforce`.source_skills ss 
										JOIN `webscale-platform-workforce`.skill_types st ON ss.skill_type_id = st.id
										WHERE st.code = '1');
                                        

#TEST LANGUAGE COUNT
IF account_group_skill_language_count = skill_language_count THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST LANGUAGE COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST LANGUAGE COUNT", "FAILED", null);
END IF;
*/
#TEST CUSTOM SKILL COUNT
IF account_group_skill_custom_count = 3 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST CUSTOM SKILL COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST CUSTOM SKILL COUNT", "FAILED", null);
END IF;


#TEST OVERALL
if overall_flag = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "PASSED", null);
ELSE 
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "FAILED", null);
END IF;

#DISPLAY RESULT
SELECT id as 'ID', test_case as 'TEST CASE', result as 'RESULT', hex(group_id) as 'GROUP ID' FROM TestResult;

DROP TEMPORARY TABLE TestResult;
END $$

DELIMITER ;