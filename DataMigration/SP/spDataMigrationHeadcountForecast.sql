Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationHeadcountForecast $$

CREATE PROCEDURE DataMigrationHeadcountForecast()
BEGIN
SET FOREIGN_KEY_CHECKS=0;

DELETE FROM `webscale-platform-workforce`.headcount_forecasts;

#Insert Headcount Forecasts
INSERT INTO `webscale-platform-workforce`.headcount_forecasts(id,group_id,task_id,start_date,end_date,count,inserted_at,updated_at,creation_user_id,update_user_id,is_volume,multiplier_configuration_id)
SELECT id,group_id,task_id,start_date,end_date,count,inserted_at,updated_at,creation_user_id,update_user_id,is_volume,multiplier_configuration_id
FROM `webscale-platform-seattle`.headcount_forecasts;

#Update Headcount Forecasts Skill
UPDATE `webscale-platform-workforce`.headcount_forecasts h1
SET h1.source_skills_id = (SELECT id from source_skills where name = 'Azerbaijani' LIMIT 1)
WHERE h1.id IN (Select h2.id 
			FROM `webscale-platform-seattle`.headcount_forecasts h2
			JOIN `webscale-platform-seattle`.skillsbases sk1
			ON h2.skill_base_id = sk1.id
			WHERE sk1.value = 'Azerbaijani');

SET FOREIGN_KEY_CHECKS=1;
END $$
DELIMITER ;