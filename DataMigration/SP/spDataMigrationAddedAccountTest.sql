Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationAddedAccountTest $$

CREATE PROCEDURE DataMigrationAddedAccountTest()
BEGIN

DECLARE test_count_configuration INT;
DECLARE test_count_contractor_client_names INT;
DECLARE test_count_job_roles INT;
DECLARE test_count_non_working_days INT;
DECLARE test_count_organization_units INT;
DECLARE test_count_profile_status INT;
DECLARE test_count_locale INT;

DECLARE overall_flag BIT;
DECLARE seattle_account_id BINARY(16);

DROP TEMPORARY TABLE IF EXISTS TestResult;
CREATE TEMPORARY TABLE TestResult (id INT NOT NULL AUTO_INCREMENT, test_case VARCHAR(255), result VARCHAR(255), group_id BINARY(16), PRIMARY KEY (id));

SET overall_flag = 1;
SET seattle_account_id = 0xe54dec54ebdb11e888e91860244727eb;

SET test_count_configuration = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.configuration) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.configuration) as tb2);

SET test_count_contractor_client_names = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.contractor_client_names WHERE account_id = seattle_account_id and active = 1) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.contractor_client_names) as tb2);

SET test_count_job_roles = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.job_roles WHERE account_id = seattle_account_id) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.job_roles) as tb2);

SET test_count_non_working_days = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.non_working_days WHERE account_id = seattle_account_id) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.non_working_days) as tb2);

SET test_count_organization_units = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.organization_units WHERE account_id = seattle_account_id and active = 1) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.organization_units) as tb2);

SET test_count_profile_status = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.profile_status WHERE account_id = seattle_account_id) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.profile_status) as tb2);

SET test_count_locale = (SELECT tb1.cnt = tb2.cnt FROM
(SELECT count(*) as cnt FROM `webscale-platform-workforce`.locale WHERE account_id = seattle_account_id) as tb1,
(SELECT count(*) as cnt FROM `webscale-platform-seattle`.locale) as tb2);

#TEST COUNT CONFIGURATION
if test_count_configuration = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT CONFIGURATION", "PASSED", null);
ELSE 
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT CONFIGURATION", "FAILED", null);
END IF;

#TEST COUNT CONTRACTOR CLIENT NAMES
if test_count_contractor_client_names = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT CONTRACTOR CLIENT NAMES", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT CONTRACTOR CLIENT NAMES", "FAILED", null);
END IF;

#TEST COUNT JOB ROLES
if test_count_job_roles = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT JOB ROLES", "PASSED", null);
ELSE
	SET overall_flag = 0; 
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT JOB ROLES", "FAILED", null);
END IF;

#TEST COUNT NON WORKING DAYS
if test_count_non_working_days = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT NON WORKING DAYS", "PASSED", null);
ELSE 
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT NON WORKING DAYS", "FAILED", null);
END IF;

#TEST COUNT ORGANIZATION UNITS
if test_count_organization_units = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT ORGANIZATION UNITS", "PASSED", null);
ELSE 
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT ORGANIZATION UNITS", "FAILED", null);
END IF;

#TEST COUNT PROFILE STATUS
if test_count_profile_status = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT PROFILE STATUS", "PASSED", null);
ELSE 
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT PROFILE STATUS", "FAILED", null);
END IF;

#TEST COUNT LOCALE
if test_count_locale = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT LOCALE", "PASSED", null);
ELSE 
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST COUNT LOCALE", "FAILED", null);
END IF;

#TEST OVERALL
if overall_flag = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "PASSED", null);
ELSE 
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "FAILED", null);
END IF;

#DISPLAY RESULT
SELECT id as 'ID', test_case as 'TEST CASE', result as 'RESULT', hex(group_id) as 'GROUP ID' FROM TestResult;

DROP TEMPORARY TABLE TestResult;
END $$

DELIMITER ;