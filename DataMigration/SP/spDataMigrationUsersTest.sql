
/** MIGRATE USERS SCRIPT **/

#CALL `webscale-platform-workforce`.`DataMigrationUsersTest`();

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationUsersTest $$

CREATE PROCEDURE DataMigrationUsersTest()

BEGIN

	DECLARE overall_flag BIT;
	DECLARE old_users_count INT;
    DECLARE new_users_count INT;
    DECLARE old_active_users_count INT;
    DECLARE new_active_users_count INT;
    DECLARE old_inactive_users_count INT;
    DECLARE new_inactive_users_count INT;
	
    SET overall_flag = 1;
    SET old_users_count = (SELECT COUNT(*) FROM `webscale-platform-seattle`.users);
    SET new_users_count = (SELECT COUNT(*) FROM `webscale-platform-workforce`.users);
    SET old_active_users_count = (SELECT COUNT(*) FROM `webscale-platform-seattle`.users WHERE active = 1);
    SET new_active_users_count = (SELECT COUNT(*) FROM `webscale-platform-workforce`.users WHERE active = 1);
    SET old_inactive_users_count = (SELECT COUNT(*) FROM `webscale-platform-seattle`.users WHERE active = 0);
    SET new_inactive_users_count = (SELECT COUNT(*) FROM `webscale-platform-workforce`.users WHERE active = 0);
    
    
	DROP TEMPORARY TABLE IF EXISTS users_test_result;
	CREATE TEMPORARY TABLE users_test_result (id INT NOT NULL AUTO_INCREMENT, test_case VARCHAR(255), result VARCHAR(255), PRIMARY KEY (id));

	#TEST USERS COUNT
	IF old_users_count = new_users_count THEN
		INSERT INTO users_test_result(test_case, result) VALUES("TEST USERS COUNT", "PASSED");
	ELSE
		SET overall_flag = 0;
		INSERT INTO users_test_result(test_case, result) VALUES("TEST USERS COUNT", "FAILED");
	END IF;

	#TEST ACTIVE USERS COUNT
	IF old_active_users_count = new_active_users_count THEN
		INSERT INTO users_test_result(test_case, result) VALUES("TEST ACTIVE USERS COUNT", "PASSED");
	ELSE
		SET overall_flag = 0;
		INSERT INTO users_test_result(test_case, result) VALUES("TEST ACTIVE USERS COUNT", "FAILED");
	END IF;

	#TEST INACTIVE USERS COUNT
	IF old_inactive_users_count = new_inactive_users_count THEN
		INSERT INTO users_test_result(test_case, result) VALUES("TEST INACTIVE USERS COUNT", "PASSED");
	ELSE
		SET overall_flag = 0;
		INSERT INTO users_test_result(test_case, result) VALUES("TEST INACTIVE USERS COUNT", "FAILED");
	END IF;
    
    #DISPLAY RESULT
    SELECT * FROM users_test_result;

END $$

DELIMITER ;