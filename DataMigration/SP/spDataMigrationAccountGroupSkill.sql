Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationAccountGroupSkill $$

CREATE PROCEDURE DataMigrationAccountGroupSkill()
BEGIN

DECLARE seattle_account_id BINARY(16);

SET FOREIGN_KEY_CHECKS=0;
SET SQL_SAFE_UPDATES=0;
SET seattle_account_id = 0xe54dec54ebdb11e888e91860244727eb;

DELETE FROM `webscale-platform-workforce`.account_group_skills WHERE active in (1,0);
/*
#Map Language Skill to Group
INSERT INTO `webscale-platform-workforce`.account_group_skills
SELECT unhex(replace(uuid(), '-', '')), ss.id, seattle_account_id, 1, now(), now(), null, null
FROM `webscale-platform-workforce`.source_skills ss
JOIN skill_types st
ON ss.skill_type_id = st.id
WHERE st.code = '1';
*/

#Map Custom Skill to Group
INSERT INTO `webscale-platform-workforce`.account_group_skills
SELECT unhex(replace(uuid(), '-', '')), ss.id, seattle_account_id, 1, now(), now(), null, null
FROM `webscale-platform-workforce`.source_skills ss
WHERE ss.name in ("Proactive", "Partner", "Phone IVR Email Alchemy");

SET FOREIGN_KEY_CHECKS=1;
SET SQL_SAFE_UPDATES=1;
END $$
DELIMITER ;