Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationSameSchema $$

CREATE PROCEDURE DataMigrationSameSchema()
BEGIN

SET FOREIGN_KEY_CHECKS=0;
SET SQL_SAFE_UPDATES=0;

#1. INSERT AGENT WORKLOAD
DELETE FROM `webscale-platform-workforce`.agent_workloads;
INSERT INTO `webscale-platform-workforce`.agent_workloads
SELECT * FROM `webscale-platform-seattle`.agent_workloads;

#2. INSERT AUDIT LOGS
DELETE FROM `webscale-platform-workforce`.audit_logs;
INSERT INTO `webscale-platform-workforce`.audit_logs
SELECT * FROM `webscale-platform-seattle`.audit_logs;

#3. INSERT CONSENT RECORDS
DELETE FROM `webscale-platform-workforce`.consent_records;
INSERT INTO `webscale-platform-workforce`.consent_records
SELECT * FROM `webscale-platform-seattle`.consent_records
WHERE created_by REGEXP '[^a-zA-Z]';

UPDATE `webscale-platform-workforce`.consent_records
SET deal_name = 'workforce';

#4. UPDATE CONSUMER
UPDATE `webscale-platform-workforce`.consumer
SET id = 'workforce', allowed_hosts = 'workforce.accenture.com', auth_providers = 'accenture', auth_redirect = 1;

#5. INSERT DATA RETENTION
DELETE FROM `webscale-platform-workforce`.data_retention;
INSERT INTO `webscale-platform-workforce`.data_retention
SELECT * FROM `webscale-platform-seattle`.data_retention;

#6. INSERT EVENT TYPES
DELETE FROM `webscale-platform-workforce`.eventtypes;
INSERT INTO `webscale-platform-workforce`.eventtypes
SELECT * FROM `webscale-platform-seattle`.eventtypes;

#7. INSERT FILE STORAGE
DELETE FROM `webscale-platform-workforce`.file_storage;
INSERT INTO `webscale-platform-workforce`.file_storage
SELECT * FROM `webscale-platform-seattle`.file_storage;

#8. INSERT MONITORING
DELETE FROM `webscale-platform-workforce`.monitoring;
INSERT INTO `webscale-platform-workforce`.monitoring
SELECT * FROM `webscale-platform-seattle`.monitoring;

#9. INSERT MONITORING CONFIGURATION
DELETE FROM `webscale-platform-workforce`.monitoring_configuration;
INSERT INTO `webscale-platform-workforce`.monitoring_configuration
SELECT * FROM `webscale-platform-seattle`.monitoring_configuration;

#10. INSERT MULTIPLIER CONFIGURATION
DELETE FROM `webscale-platform-workforce`.multiplier_configuration;
INSERT INTO `webscale-platform-workforce`.multiplier_configuration
SELECT *, '0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0', 1 FROM `webscale-platform-seattle`.multiplier_configuration;

#BUG FIX
DELETE FROM `webscale-platform-workforce`.multiplier_configuration 
WHERE id = 0x0328D962FF8D498BB569CFE4A57FEDDE;

#11. INSERT OTHER ROLES
DELETE FROM `webscale-platform-workforce`.other_roles;
INSERT INTO `webscale-platform-workforce`.other_roles
SELECT * FROM `webscale-platform-seattle`.other_roles;

#12. INSERT REFERENCE DATA
DELETE FROM `webscale-platform-workforce`.reference_data;
INSERT INTO `webscale-platform-workforce`.reference_data
SELECT * FROM `webscale-platform-seattle`.reference_data;

#13. INSERT REFERENCE DATA TYPES
DELETE FROM `webscale-platform-workforce`.reference_data_types;
INSERT INTO `webscale-platform-workforce`.reference_data_types
SELECT * FROM `webscale-platform-seattle`.reference_data_types;

#14. INSERT SHIFT RULES
DELETE FROM `webscale-platform-workforce`.shift_rules;
INSERT INTO `webscale-platform-workforce`.shift_rules
SELECT * FROM `webscale-platform-seattle`.shift_rules;

#15. INSERT SHIFT TEMPLATE
DELETE FROM `webscale-platform-workforce`.shift_template;
INSERT INTO `webscale-platform-workforce`.shift_template
SELECT * FROM `webscale-platform-seattle`.shift_template;

#16. INSERT SLA SNAPSHOT
DELETE FROM `webscale-platform-workforce`.sla_snapshot;
INSERT INTO `webscale-platform-workforce`.sla_snapshot
SELECT * FROM `webscale-platform-seattle`.sla_snapshot;

#17. INSERT TASKS
DELETE FROM `webscale-platform-workforce`.tasks;
INSERT INTO `webscale-platform-workforce`.tasks
SELECT * FROM `webscale-platform-seattle`.tasks;

#18. INSERT UPLOADED FILES
DELETE FROM `webscale-platform-workforce`.uploaded_files;
INSERT INTO `webscale-platform-workforce`.uploaded_files
SELECT * FROM `webscale-platform-seattle`.uploaded_files;

#19. INSERT VOLUMES
DELETE FROM `webscale-platform-workforce`.volumes;
INSERT INTO `webscale-platform-workforce`.volumes
SELECT * FROM `webscale-platform-seattle`.volumes;

#20. INSERT VOLUME SETS
DELETE FROM `webscale-platform-workforce`.volume_sets;
INSERT INTO `webscale-platform-workforce`.volume_sets
SELECT * FROM `webscale-platform-seattle`.volume_sets;

#BUG FIX

Update `webscale-platform-workforce`.consent_messages
Set message = '<html>
 	<head>
 	<h4><strong>Data Privacy Statement</strong><br>
 	</h4>
 	</head>
 	<body>
 	<p>Please be aware that any personal data that you and others provide through this tool may be processed by Accenture. <br>
 	The protection of your personal data is very important to Accenture. Accenture is committed to keeping your personal data secure, 
 	and processing it in accordance with, applicable data protection laws and our internal policies, including 
 	<a href="https://policies.accenture.com/policy/0090?rd=1&Country=United%20Kingdom" target="_blank">Accenture\'s Global Data Privacy Policy 0090</a>.<br><br>
 	Your decision to provide your personal data to Accenture is voluntary. However, given that this tool requires personal data to function, 
 	you will not be able to use this tool if you do not provide your personal data.<br>
 	Before providing any personal data through this tool, Accenture invites you to carefully read its 
 	<a href="https://in.accenture.com/protectingaccenture/data-security/5422-2/" target="_blank">privacy statement</a>, which 
 	includes important information on why and how Accenture is processing your personal data.</p>
 	</body>
 	<br>
 	<head>
 	<h4><strong>Legal Notice</strong><br>
 	</h4>
 	</head>
 	<body>
 	<p>This system is the property of Accenture, and is to be used in accordance with applicable Accenture Policies. Unauthorized access or activity is a violation of Accenture Policies and may be a violation of law. Use of this system constitutes consent to monitoring for unauthorized use, in accordance with Accenture Policies, local laws, and regulations.
 	 Unauthorized use may result in penalties including, but not limited to, reprimand, dismissal, financial penalties, and legal action.</p>
 	</body>
 	</html>'
where id = 1;

SET FOREIGN_KEY_CHECKS=1;
SET SQL_SAFE_UPDATES=1;

END $$

DELIMITER ;