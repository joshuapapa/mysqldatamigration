Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationSLATest $$

CREATE PROCEDURE DataMigrationSLATest()
BEGIN

DECLARE test_old_sla_count INT;
DECLARE test_new_sla_count INT;
DECLARE missing_skill_count INT;
DECLARE overall_flag BIT;

DROP TEMPORARY TABLE IF EXISTS TestResult;
CREATE TEMPORARY TABLE TestResult (id INT NOT NULL AUTO_INCREMENT, test_case VARCHAR(255), result VARCHAR(255), group_id BINARY(16), PRIMARY KEY (id));

SET overall_flag = 1;
SET test_old_sla_count = (Select count(*) from `webscale-platform-seattle`.sla);
SET test_new_sla_count = (Select count(*) from `webscale-platform-workforce`.sla);
SET missing_skill_count = (Select count(*) from `webscale-platform-workforce`.sla where sla.source_skills_id is null);

#TEST SLA COUNT
IF test_old_sla_count = test_new_sla_count THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST SLA COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST SLA COUNT", "FAILED", null);
END IF;

#TEST MISSING SLA SKILL COUNT
IF missing_skill_count = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST MISSING SLA SKILL COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST MISSING SLA SKILL COUNT", "FAILED", null);
END IF;

#TEST OVERALL
if overall_flag = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "PASSED", null);
ELSE 
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "FAILED", null);
END IF;

#DISPLAY RESULT
SELECT id as 'ID', test_case as 'TEST CASE', result as 'RESULT', hex(group_id) as 'GROUP ID' FROM TestResult;

DROP TEMPORARY TABLE TestResult;
END $$

DELIMITER ;