Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationEventTest $$

CREATE PROCEDURE DataMigrationEventTest()
BEGIN

DECLARE old_event_count INT;
DECLARE new_event_count INT;
DECLARE old_event_count_has_skill INT;
DECLARE new_event_count_has_skill INT;
DECLARE old_event_skill_count INT;
DECLARE new_event_skill_count INT;
DECLARE missed_total_skill_count INT;
DECLARE overall_flag BIT;

SET FOREIGN_KEY_CHECKS=0;

DROP TEMPORARY TABLE IF EXISTS TestResult;
CREATE TEMPORARY TABLE TestResult (id INT NOT NULL AUTO_INCREMENT, test_case VARCHAR(255), result VARCHAR(255), group_id BINARY(16), PRIMARY KEY (id));

SET overall_flag = 1;
SET old_event_count = (SELECT count(id) FROM `webscale-platform-seattle`.events);
SET new_event_count = (SELECT count(id) FROM `webscale-platform-workforce`.events);
SET old_event_count_has_skill = (SELECT count(id) FROM `webscale-platform-seattle`.events WHERE skill_base_id is not null);
SET new_event_count_has_skill = (SELECT count(id) FROM `webscale-platform-workforce`.events WHERE source_skills_id is not null);
SET old_event_skill_count = (SELECT count(skill_id) FROM (SELECT DISTINCT(skill_base_id) as skill_id FROM `webscale-platform-seattle`.events) sk);
SET new_event_skill_count = (SELECT count(skill_id) FROM (SELECT DISTINCT(source_skills_id) as skill_id FROM `webscale-platform-workforce`.events) sk);

SET missed_total_skill_count = (SELECT count(e1.id) 
	FROM `webscale-platform-workforce`.events as e1
	JOIN `webscale-platform-workforce`.source_skills as s1
	ON e1.source_skills_id = s1.id
	WHERE NOT EXISTS(
		SELECT 1 FROM `webscale-platform-seattle`.events as e2
		WHERE e1.id = e2.id AND s1.name = (SELECT sb.value 
			FROM  `webscale-platform-seattle`.skillsbases as sb
			WHERE sb.id = e2.skill_base_id)
	));
    
#TEST EVENT COUNT
IF old_event_count = new_event_count THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST EVENT COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST EVENT COUNT", "FAILED", null);
END IF;

#TEST EVENT SKILL COUNT
IF old_event_count_has_skill = new_event_count_has_skill THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST EVENT SKILL COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST EVENT SKILL COUNT", "FAILED", null);
END IF;

#TEST SKILL COUNT
IF old_event_skill_count = new_event_skill_count THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST SKILL COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST SKILL COUNT", "FAILED", null);
END IF;

#TEST MISSED SKILL COUNT
IF missed_total_skill_count = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST MISSED SKILL COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id)
		SELECT "TEST MISSED SKILL", "FAILED", e1.id 
		FROM `webscale-platform-workforce`.events as e1
		JOIN `webscale-platform-workforce`.source_skills as s1
		ON e1.source_skills_id = s1.id
		WHERE NOT EXISTS(
			SELECT 1 FROM `webscale-platform-seattle`.events as e2
			WHERE e1.id = e2.id AND s1.name = (SELECT sb.value 
				FROM  `webscale-platform-seattle`.skillsbases as sb
				WHERE sb.id = e2.skill_base_id)
		);
END IF;

#TEST OVERALL
if overall_flag = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "PASSED", null);
ELSE 
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "FAILED", null);
END IF;

#DISPLAY RESULT
SELECT id as 'ID', test_case as 'TEST CASE', result as 'RESULT', hex(group_id) as 'EVENT ID' FROM TestResult;

DROP TEMPORARY TABLE TestResult;

SET FOREIGN_KEY_CHECKS=1;
END $$
DELIMITER ;