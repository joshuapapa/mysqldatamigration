
/** SOURCE SKILL DATA MIGRATION SCRIPT **/

#CALL `webscale-platform-workforce`.`DataMigrationSourceSkills`();


DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationSourceSkills $$

CREATE PROCEDURE DataMigrationSourceSkills()

BEGIN

	SET FOREIGN_KEY_CHECKS = 0;
    SET SQL_SAFE_UPDATES = 0;
	
    DELETE FROM `webscale-platform-workforce`.source_skills
	WHERE name in ("Proactive","Partner","Phone IVR Email Alchemy");

	INSERT INTO `webscale-platform-workforce`.source_skills(
		id
		, name
		, code
		, description
		, skill_type_id
		, source
		, source_id
		, active
		, inserted_at
		, updated_at
		, creation_user_id
		, update_user_id
	)
	SELECT	
		UNHEX(REPLACE(UUID(), "-",""))
		, s.value
		, NULL
		, NULL
		, CASE
			WHEN s.skilltype = 'Language'
			THEN (SELECT id from `webscale-platform-workforce`.skill_types where code = '1')
			ELSE (SELECT id from `webscale-platform-workforce`.skill_types where code = '0')
		  END
		, 0 
		, NULL
		, s.isActive
		, now()
		, now()
		, NULL
		, NULL
	FROM (
		SELECT
			distinct sb.value
			, st.name as skilltype
			, sb.active as isActive
		FROM `webscale-platform-seattle`.skillsbases sb
		LEFT JOIN `webscale-platform-workforce`.source_skills ss
			ON sb.value = ss.name
		LEFT JOIN `webscale-platform-workforce`.skill_types st
			ON sb.skill_type_id = st.id
		WHERE ss.code IS NULL
		AND sb.active = 1
		AND sb.id IN (select skill_base_id from `webscale-platform-seattle`.skills)
	) AS s
	WHERE s.value NOT IN (SELECT name FROM `webscale-platform-workforce`.source_skills WHERE name LIKE '%s.value%');
	
	SET FOREIGN_KEY_CHECKS = 1;
    SET SQL_SAFE_UPDATES = 1;

END $$

DELIMITER ;

    