Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationSkill $$

CREATE PROCEDURE DataMigrationSkill()
BEGIN

SET FOREIGN_KEY_CHECKS=0;
SET SQL_SAFE_UPDATES = 0;

DELETE FROM `webscale-platform-workforce`.source_skills
WHERE name in ("Proactive","Partner","Phone IVR Email Alchemy");

#Insert Source Skills
INSERT INTO `webscale-platform-workforce`.source_skills(id,name,code,description,skill_type_id,source,source_id,active,inserted_at,updated_at,creation_user_id,update_user_id)
VALUES(unhex(replace(uuid(), '-', '')), "Proactive", "", "", (Select id from skill_types where name = "Skill"),0,null,1,now(),now(),null,null),
(unhex(replace(uuid(), '-', '')), "Partner", "", "", (Select id from skill_types where name = "Skill"),0,null,1,now(),now(),null,null),
(unhex(replace(uuid(), '-', '')), "Phone IVR Email Alchemy", "", "", (Select id from skill_types where name = "Skill"),0,null,1,now(),now(),null,null);

SET FOREIGN_KEY_CHECKS=1;
SET SQL_SAFE_UPDATES = 1;
END $$
DELIMITER ;