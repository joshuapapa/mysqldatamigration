Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationACNProfileTest $$

CREATE PROCEDURE DataMigrationACNProfileTest()
BEGIN

DECLARE old_user_location_count INT;
DECLARE new_user_location_count INT;
DECLARE new_user_acn_profile_count INT;
DECLARE missing_location_count INT;
DECLARE overall_flag BIT;

DROP TEMPORARY TABLE IF EXISTS TestResult;
CREATE TEMPORARY TABLE TestResult (id INT NOT NULL AUTO_INCREMENT, test_case VARCHAR(255), result VARCHAR(255), group_id BINARY(16), PRIMARY KEY (id));

SET old_user_location_count = (SELECT count(*) FROM `webscale-platform-seattle`.users WHERE location_id is not null);
SET new_user_location_count = (SELECT count(*) FROM `webscale-platform-workforce`.users WHERE acn_profile_id is not null);
SET new_user_acn_profile_count = (SELECT count(*) FROM `webscale-platform-workforce`.acn_profile);
SET missing_location_count = (SELECT count(*) FROM `webscale-platform-workforce`.acn_profile WHERE geographic_region is NULL);

SET overall_flag = 1;

#TEST ACN PROFILE FK COUNT
IF old_user_location_count = new_user_location_count THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST ACN PROFILE FK COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST ACN PROFILE FK COUNT", "FAILED", null);
END IF;

#TEST ACN PROFILE COUNT
IF old_user_location_count = new_user_acn_profile_count THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST ACN PROFILE COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST ACN PROFILE COUNT", "FAILED", null);
END IF;

#TEST MISSING LOCATION COUNT
IF missing_location_count = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST MISSING LOCATION COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id)
    SELECT "TEST MISSING LOCATION", "FAILED", a.id from `webscale-platform-workforce`.acn_profile as a where geographic_region is null;
END IF;

#TEST OVERALL
if overall_flag = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "PASSED", null);
ELSE 
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "FAILED", null);
END IF;

#DISPLAY RESULT
SELECT id as 'ID', test_case as 'TEST CASE', result as 'RESULT', hex(group_id) as 'ACN Profile ID' FROM TestResult;

DROP TEMPORARY TABLE TestResult;
END $$

DELIMITER ;