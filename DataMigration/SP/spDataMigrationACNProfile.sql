Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationACNProfile $$

CREATE PROCEDURE DataMigrationACNProfile()
BEGIN

SET FOREIGN_KEY_CHECKS=0;
SET SQL_SAFE_UPDATES=0;

DELETE FROM `webscale-platform-workforce`.acn_profile;

Update `webscale-platform-workforce`.users as u
Set u.acn_profile_id = null
WHERE u.active in (0,1);

#Update User ACN Profile
Update `webscale-platform-workforce`.users as u
Set u.acn_profile_id = unhex(replace(uuid(), '-', ''))
WHERE u.acn_profile_id is null AND 
	u.id IN (SELECT u2.id FROM `webscale-platform-seattle`.users as u2 WHERE u2.location_id is not null);

#Insert ACN Profile
INSERT `webscale-platform-workforce`.acn_profile
SELECT u.acn_profile_id,null,null,null,null,null,null,null,now(),now(),null,null from `webscale-platform-workforce`.users u
WHERE u.acn_profile_id is not null;

#Update ACN Profile
Update `webscale-platform-workforce`.acn_profile as a1
Set a1.geographic_region = 'AAPAC', a1.geographic_unit = 'ASEAN', a1.country = 'Malaysia', a1.metro_city = 'Kuala Lumpur', a1.city = 'Kuala Lumpur'
where a1.id in (Select u1.acn_profile_id 
	from `webscale-platform-workforce`.users as u1
    JOIN `webscale-platform-seattle`.users as u2
    ON u1.id = u2.id
    JOIN `webscale-platform-seattle`.locations as l1
    ON u2.location_id = l1.id
    where l1.long_name = 'Kuala Lumpur'
);

Update `webscale-platform-workforce`.acn_profile as a1
Set a1.geographic_region = 'AAPAC', a1.geographic_unit = 'ASEAN', a1.country = 'Philippines', a1.metro_city = 'ManilaCity', a1.city = 'Quezon', a1.facility = 'Quezon Global One Center'
where a1.id in (Select u1.acn_profile_id
	from `webscale-platform-workforce`.users as u1
    JOIN `webscale-platform-seattle`.users as u2
    ON u1.id = u2.id
    JOIN `webscale-platform-seattle`.locations as l1
    ON u2.location_id = l1.id
    where l1.long_name = 'Global One'
);

Update `webscale-platform-workforce`.acn_profile as a1
Set a1.geographic_region = 'North America', a1.geographic_unit = 'United States', a1.country = 'USA'
where a1.id in (Select u1.acn_profile_id
	from `webscale-platform-workforce`.users as u1
    JOIN `webscale-platform-seattle`.users as u2
    ON u1.id = u2.id
    JOIN `webscale-platform-seattle`.locations as l1
    ON u2.location_id = l1.id
    where l1.long_name = 'US'
);

Update `webscale-platform-workforce`.acn_profile as a1
Set a1.geographic_region = 'Europe', a1.geographic_unit = 'ICEG', a1.country = 'Slovakia', a1.metro_city = 'Bratislava', a1.city = 'Bratislava'
where a1.id in (Select u1.acn_profile_id
	from `webscale-platform-workforce`.users as u1
    JOIN `webscale-platform-seattle`.users as u2
    ON u1.id = u2.id
    JOIN `webscale-platform-seattle`.locations as l1
    ON u2.location_id = l1.id
    where l1.long_name = 'Bratislava'
);

Update `webscale-platform-workforce`.acn_profile as a1
Set a1.geographic_region = 'AAPAC', a1.geographic_unit = 'ASEAN', a1.country = 'Philippines'
where a1.id in (Select u1.acn_profile_id
	from `webscale-platform-workforce`.users as u1
    JOIN `webscale-platform-seattle`.users as u2
    ON u1.id = u2.id
    JOIN `webscale-platform-seattle`.locations as l1
    ON u2.location_id = l1.id
    where l1.long_name = 'PH'
);

Update `webscale-platform-workforce`.acn_profile as a1
Set a1.geographic_region = 'AAPAC', a1.geographic_unit = 'ASEAN', a1.country = 'Philippines', a1.metro_city = 'ManilaCity', a1.city = 'Taguig', a1.facility = 'Taguig Uptown Bonifacio Tower 3'
where a1.id in (Select u1.acn_profile_id
	from `webscale-platform-workforce`.users as u1
    JOIN `webscale-platform-seattle`.users as u2
    ON u1.id = u2.id
    JOIN `webscale-platform-seattle`.locations as l1
    ON u2.location_id = l1.id
    where l1.long_name = 'Uptown 3'
);

Update `webscale-platform-workforce`.acn_profile as a1
Set a1.geographic_region = 'AAPAC', a1.geographic_unit = 'ASEAN', a1.country = 'Philippines', a1.metro_city = 'CebuCity', a1.city = 'Cebu'
	where a1.id in (Select u1.acn_profile_id
	from `webscale-platform-workforce`.users as u1
    JOIN `webscale-platform-seattle`.users as u2
    ON u1.id = u2.id
    JOIN `webscale-platform-seattle`.locations as l1
    ON u2.location_id = l1.id
    where l1.long_name = 'Cebu'
);

Update `webscale-platform-workforce`.acn_profile as a1
Set a1.geographic_region = 'Europe', a1.geographic_unit = 'ICEG', a1.country = 'Slovakia'
where a1.id in (Select u1.acn_profile_id
	from `webscale-platform-workforce`.users as u1
    JOIN `webscale-platform-seattle`.users as u2
    ON u1.id = u2.id
    JOIN `webscale-platform-seattle`.locations as l1
    ON u2.location_id = l1.id
    where l1.long_name = 'SVK'
);

Update `webscale-platform-workforce`.acn_profile as a1
Set a1.geographic_region = 'Latin America', a1.geographic_unit = 'Latin America', a1.country = 'Mexico'
where a1.id in (Select u1.acn_profile_id
	from `webscale-platform-workforce`.users as u1
    JOIN `webscale-platform-seattle`.users as u2
    ON u1.id = u2.id
    JOIN `webscale-platform-seattle`.locations as l1
    ON u2.location_id = l1.id
    where l1.long_name = 'MX'
);

SET FOREIGN_KEY_CHECKS=1;
SET SQL_SAFE_UPDATES=1;

END $$

DELIMITER ;