Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationHeadcountForecastTest $$

CREATE PROCEDURE DataMigrationHeadcountForecastTest()
BEGIN

DECLARE test_old_hf_count INT;
DECLARE test_new_hf_count INT;
DECLARE missing_skill_count INT;
DECLARE overall_flag BIT;

DROP TEMPORARY TABLE IF EXISTS TestResult;
CREATE TEMPORARY TABLE TestResult (id INT NOT NULL AUTO_INCREMENT, test_case VARCHAR(255), result VARCHAR(255), group_id BINARY(16), PRIMARY KEY (id));

SET overall_flag = 1;
SET test_old_hf_count = (Select count(*) from `webscale-platform-seattle`.headcount_forecasts);
SET test_new_hf_count = (Select count(*) from `webscale-platform-workforce`.headcount_forecasts);
SET missing_skill_count = (Select count(*) from `webscale-platform-workforce`.headcount_forecasts where headcount_forecasts.source_skills_id is null);

#TEST HEADCOUNT FORECAST COUNT
IF test_old_hf_count = test_new_hf_count THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST HEADCOUNT FORECAST COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST HEADCOUNT FORECAST COUNT", "FAILED", null);
END IF;

#TEST HEADCOUNT FORECAST SKILL COUNT
IF missing_skill_count = 0 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST MISSING HEADCOUNT FORECAST SKILL COUNT", "PASSED", null);
ELSE
	SET overall_flag = 0;
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST MISSING HEADCOUNT FORECAST SKILL COUNT", "FAILED", null);
END IF;

#TEST OVERALL
if overall_flag = 1 THEN
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "PASSED", null);
ELSE 
	INSERT INTO TestResult(test_case, result, group_id) VALUES("TEST OVERALL", "FAILED", null);
END IF;

#DISPLAY RESULT
SELECT id as 'ID', test_case as 'TEST CASE', result as 'RESULT', hex(group_id) as 'GROUP ID' FROM TestResult;

DROP TEMPORARY TABLE TestResult;
END $$

DELIMITER ;