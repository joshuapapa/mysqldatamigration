Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationAddedAccount $$

CREATE PROCEDURE DataMigrationAddedAccount()
BEGIN

DECLARE seattle_account_id BINARY(16);

SET FOREIGN_KEY_CHECKS=0;
SET SQL_SAFE_UPDATES=0;
SET seattle_account_id = 0xe54dec54ebdb11e888e91860244727eb;

#1. INSERT CONFIGURATION
DELETE FROM `webscale-platform-workforce`.configuration;
INSERT INTO `webscale-platform-workforce`.configuration(id,name,value,inserted_at,updated_at,creation_user_id,update_user_id)
SELECT id,name,value,inserted_at,updated_at,creation_user_id,update_user_id FROM `webscale-platform-seattle`.configuration;

UPDATE `webscale-platform-workforce`.configuration
SET account_id = seattle_account_id
WHERE name IN ('FTE_ACCRUED_RESET_DATE', 'CONTRACTOR_ACCRUED_RESET_DATE', 'MINS_BEFORE_SHIFT_START');

#2. INSERT CONTRACTOR CLIENT NAMES
DELETE FROM `webscale-platform-workforce`.contractor_client_names;
INSERT INTO `webscale-platform-workforce`.contractor_client_names(id,name,inserted_at,updated_at,creation_user_id,update_user_id)
SELECT id,name,inserted_at,updated_at,creation_user_id,update_user_id FROM `webscale-platform-seattle`.contractor_client_names;

UPDATE `webscale-platform-workforce`.contractor_client_names
SET account_id = seattle_account_id, active = 1;

#3. INSERT JOB ROLES
DELETE FROM `webscale-platform-workforce`.job_roles;
INSERT INTO `webscale-platform-workforce`.job_roles(id,name,description,active,inserted_at,updated_at,creation_user_id,update_user_id)
SELECT id,name,description,active,inserted_at,updated_at,creation_user_id,update_user_id FROM `webscale-platform-seattle`.job_roles;

UPDATE `webscale-platform-workforce`.job_roles
SET account_id = seattle_account_id;

#4. INSERT NON WORKING DAYS
DELETE FROM `webscale-platform-workforce`.non_working_days;
INSERT INTO `webscale-platform-workforce`.non_working_days(id,name,event_date,timezone,active,inserted_at,updated_at,creation_user_id,update_user_id)
SELECT id,name,event_date,timezone,active,inserted_at,updated_at,creation_user_id,update_user_id FROM `webscale-platform-seattle`.non_working_days;

UPDATE `webscale-platform-workforce`.non_working_days
SET account_id = seattle_account_id;

#5. INSERT ORGANIZATION UNITS
DELETE FROM `webscale-platform-workforce`.organization_units;
INSERT INTO `webscale-platform-workforce`.organization_units(id,name,inserted_at,updated_at,creation_user_id,update_user_id)
SELECT id,name,inserted_at,updated_at,creation_user_id,update_user_id FROM `webscale-platform-seattle`.organization_units;

UPDATE `webscale-platform-workforce`.organization_units
SET account_id = seattle_account_id, active = 1;

#6. INSERT PROFILE STATUS
DELETE FROM `webscale-platform-workforce`.profile_status;
INSERT INTO `webscale-platform-workforce`.profile_status(id,name,active,inserted_at,updated_at)
SELECT id,name,active,inserted_at,updated_at FROM `webscale-platform-seattle`.profile_status;

UPDATE `webscale-platform-workforce`.profile_status
SET account_id = seattle_account_id;

#7. INSERT LOCALE
DELETE FROM `webscale-platform-workforce`.locale;
INSERT INTO `webscale-platform-workforce`.locale(id,name,inserted_at,updated_at,creation_user_id,update_user_id)
SELECT id,name,inserted_at,updated_at,creation_user_id,update_user_id FROM `webscale-platform-seattle`.locale;

UPDATE `webscale-platform-workforce`.locale
SET account_id = seattle_account_id;

SET FOREIGN_KEY_CHECKS=1;
SET SQL_SAFE_UPDATES=1;

END $$

DELIMITER ;