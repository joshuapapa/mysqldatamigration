
/** MIGRATE USERS SCRIPT **/

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationUserGroupRolesTest $$

CREATE PROCEDURE DataMigrationUserGroupRolesTest()

BEGIN

	DECLARE overall_flag BIT;
	DECLARE old_memberships_count INT;
    DECLARE old_user_roles_count INT;
    DECLARE old_users_nonmember_count INT;
    DECLARE user_group_roles_count INT;
    
    
	SET old_memberships_count = (SELECT COUNT(*) FROM `webscale-platform-workforce`.membership_temp);
	SET old_user_roles_count = (SELECT COUNT(DISTINCT user_id) FROM `webscale-platform-seattle`.user_roles);
    SET old_users_nonmember_count = (SELECT COUNT(DISTINCT user_id) FROM `webscale-platform-seattle`.user_roles
									WHERE user_id NOT IN (SELECT user_id FROM `webscale-platform-seattle`.membership));
	SET user_group_roles_count = old_memberships_count + old_user_roles_count;
	
	DROP TEMPORARY TABLE IF EXISTS usersgrouproles_test_result;
	CREATE TEMPORARY TABLE usersgrouproles_test_result (id INT NOT NULL AUTO_INCREMENT, test_case VARCHAR(255), result VARCHAR(255), PRIMARY KEY (id));

	#TEST USERS GROUP ROLES COUNT
	IF user_group_roles_count = (SELECT COUNT(*) FROM .user_group_roles) THEN
		INSERT INTO usersgrouproles_test_result(test_case, result) VALUES("TEST USERS COUNT", "PASSED");
	ELSE
		SET overall_flag = 0;
		INSERT INTO usersgrouproles_test_result(test_case, result) VALUES("TEST USERS COUNT", "FAILED");
	END IF;
    
    #DISPLAY RESULT
    SELECT * FROM usersgrouproles_test_result;

END $$

DELIMITER ;