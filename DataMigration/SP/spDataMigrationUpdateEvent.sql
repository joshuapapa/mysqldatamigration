Use `webscale-platform-workforce`;

DELIMITER $$

DROP PROCEDURE IF EXISTS DataMigrationUpdateEvent $$

CREATE PROCEDURE DataMigrationUpdateEvent()
BEGIN

SET FOREIGN_KEY_CHECKS=0;
SET SQL_SAFE_UPDATES=0;

#Update Event Source Skills
UPDATE `webscale-platform-workforce`.events e1
SET e1.source_skills_id = (SELECT id FROM source_skills WHERE name = "Proactive")
WHERE e1.id in (SELECT e2.id FROM `webscale-platform-seattle`.events e2
	JOIN `webscale-platform-seattle`.skillsbases sk1
    ON e2.skill_base_id = sk1.id
    WHERE sk1.value = "Proactive");

UPDATE `webscale-platform-workforce`.events e1
SET e1.source_skills_id = (SELECT id FROM source_skills WHERE name = "Partner")
WHERE e1.id in (SELECT e2.id FROM `webscale-platform-seattle`.events e2
	JOIN `webscale-platform-seattle`.skillsbases sk1
    ON e2.skill_base_id = sk1.id
    WHERE sk1.value = "Partner");
    
UPDATE `webscale-platform-workforce`.events e1
SET e1.source_skills_id = (SELECT id FROM source_skills WHERE name = "Phone IVR Email Alchemy")
WHERE e1.id in (SELECT e2.id FROM `webscale-platform-seattle`.events e2
	JOIN `webscale-platform-seattle`.skillsbases sk1
    ON e2.skill_base_id = sk1.id
    WHERE sk1.value = "Phone IVR Email Alchemy");
    
SET FOREIGN_KEY_CHECKS=1;
SET SQL_SAFE_UPDATES=1;
END $$
DELIMITER ;